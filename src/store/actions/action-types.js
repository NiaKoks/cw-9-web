export const ORDER_REQUEST='ORDER_REQUEST';
export const ORDER_SUCCESS ='ORDER_SUCCESS';
export const ORDER_FAILURE ='ORDER_FAILURE';

export const OPEN_MODAL ='OPEN_MODAL';
export const CLOSE_MODAL ='CLOSE_MODAL';

export const ADD_CONTACT = 'ADD_CONTACT';
export const EDIT_CONTACT = 'EDIT_CONTACT';
export const DEL_CONTACT = 'DEL_CONTACT';
