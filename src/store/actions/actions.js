import axios from '../../axios-contacts';
import {CLOSE_MODAL, OPEN_MODAL, ORDER_FAILURE, ORDER_REQUEST, ORDER_SUCCESS} from "./action-types";

export const orderReq = () =>({type: ORDER_REQUEST});
export const orderSuc = allContacts => ({type: ORDER_SUCCESS,allContacts});
export const orderFail = error =>({type: ORDER_FAILURE});

export const getContacts = contact=>{
    return dispatch =>{
        dispatch(orderReq());
        axios.get('/contacts.json').then(responce =>{
                dispatch(orderSuc(responce.data))},error => dispatch(orderFail(error))
    )
    }
}
export const addContact= contact =>{
    return dispatch =>{
        dispatch(orderReq());
        return axios.post('/contacts.json',contact)
    }
};
export const editContact = contact =>{
    const id = this.props.match.params.id;
    axios.put(`/contacts/${id}.json`,contact)
};
export const delContact =(contactData)=>{
    return dispatch =>{
        dispatch(orderReq());
        axios.delete('/contacts'+contactData+'.json').then(() =>{
            dispatch(getContacts());
        },error => dispatch(orderFail(error)))
    }
};
export function openModal(){
    return {type:OPEN_MODAL};
}
export function closeModal(){
    return {type:CLOSE_MODAL};
}