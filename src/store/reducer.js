import {
    ADD_CONTACT,
    DEL_CONTACT,
    EDIT_CONTACT,
    ORDER_REQUEST,
    ORDER_SUCCESS,
    ORDER_FAILURE, OPEN_MODAL, CLOSE_MODAL
} from './actions/action-types'

const initialState ={
    allContacts: {},
    oneContact: {},
    loading: false,
    error: null,
    modalOpen: false,
};

const contactReducer =(state = initialState,action)=>{
    switch (action.type) {
        case ORDER_REQUEST:
            return{
                ...state,
                loading: true,
            };
        case ORDER_SUCCESS:
            return{
                ...state,
                loading:false,
                allContacts: action.allContacts,
            };
        case ORDER_FAILURE:
            return{
                ...state,
                loading:false,
                error: action.error
            };
        case ADD_CONTACT:
            return{
                ...state,
                allContacts:{
                    ...state.allContacts,
                    [action.name]:state.allContacts[action.name] + 1,
                    [action.number]:state.allContacts[action.number] +1,
                    [action.email]:state.allContacts[action.email] +1,
                    [action.img]:state.allContacts[action.img] +1,
                }
            };
        case EDIT_CONTACT:
            return{
                ...state,
                oneContact:{
                    [action.name]:state.oneContact[action.name],
                    [action.number]:state.oneContact[action.number],
                    [action.email]:state.oneContact[action.email],
                    [action.img]:state.oneContact[action.img],
                }
            };
        case DEL_CONTACT:
            if (state.allContacts[action.name] >0){
                return{
                    ...state,
                    allContacts: {
                        ...state.allContacts,
                        [action.name]: state.allContacts[action.name] -1,
                        [action.number]: state.allContacts[action.number] -1,
                        [action.email]: state.allContacts[action.email] -1,
                        [action.img]: state.allContacts[action.img] -1,
                    }
                }
            } else return state;
        case OPEN_MODAL:
            return{
                ...state,
                modalOpen: true
            };
        case CLOSE_MODAL:
            return{
                ...state,
                modalOpen: false
            };
        default:
            return state

    }
}

export default contactReducer
