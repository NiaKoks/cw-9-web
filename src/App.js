import React, { Component } from 'react';
import AddForm from './Components/AddForm/AddForm'
import ContactBook from './Containers/ContactBook'
import './App.css';
import {BrowserRouter,Switch,Route} from "react-router-dom";
import Add from "./Containers/Add/Add";

class App extends Component {
  render() {
    return (
        <BrowserRouter>
          <Switch>
            <Route path="/" exact component={ContactBook}/>
            <Route path="/add" exact component={Add}/>
            <Route render={()=><h1>Not Found!</h1>} />
            <Route path="/edit/:id" component={AddForm}></Route>
          </Switch>
        </BrowserRouter>
    );
  }
}

export default App;
