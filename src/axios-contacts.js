import axios from 'axios'

const instance =axios.create({
    baseURL: 'https://cw-9-nia.firebaseio.com'
});

export default instance;