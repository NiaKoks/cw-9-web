import React, {Component} from 'react';

class Modal extends Component {
    render() {
        const {modalOpen} = this.state.props;

        if (!modalOpen) return null;
        return (
            <div className="Backdrop">
                <div className="Modal">
                    <img src={this.props.img} alt="photo here"/>
                    <p><b>Name:</b>{this.props.name}</p>
                    <p><b>Phone Number:</b>{this.props.number}</p>
                    <p><b>E-mail:</b>{this.props.email}</p>
                    <button onClick={this.props.addContact}>Edit</button>
                    <button>X</button>
                </div>
            </div>
        );
    }
}

export default Modal;