import React, {Component} from 'react';
import './AddForm.css'
import {NavLink} from "react-router-dom";
class AddForm extends Component {
    constructor(props){
        super(props);
        if(props.contacts){
            this.state = {...props.contacts}
        }else{
            this.state= {
                name:'',
                email:'',
                number: '',
                img: ''
            }
        }
    }
    valueChanged =(event) =>{
        const{name , value} = event.target;
        this.setState({[name]:value})
    };
    submitHandler = event=>{
        console.log('hljjl');
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };


    render() {
        return (
            <form onSubmit={this.submitHandler} className="AddContactForm">
                <input type="text" name='name'
                    value={this.state.name} onChange={this.valueChanged}
                    placeholder="Input name of the Contact"/>
                    <input type="text" name='number'
                    value={this.state.number} onChange={this.valueChanged}
                           placeholder="Input number of the Contact"/>
                    <input type="text" name='email'
                    value={this.state.email} onChange={this.valueChanged}
                           placeholder="Input email of the Contact"/>
                    <input type="text" name='img'
                    value={this.state.img} onChange={this.valueChanged}
                           placeholder="Add photo of the Contact"/>
                    <button type="submit">ADD</button>
            </form>
        );
    }
}

export default AddForm;