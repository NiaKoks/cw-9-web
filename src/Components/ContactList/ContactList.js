import React, {Component,Fragment} from 'react';
import {connect} from 'react-redux';
import {delContact, getContacts, openModal} from "../../store/actions/actions";
import './ContactList.css';

class ContactList extends Component {
    componentDidMount() {
        this.props.getContacts();
        console.log(this.props.contacts);
    };
    render() {
        return (
            <div>
                <Fragment>

                    {Object.keys(this.props.contacts).map((contact, index) => {
                        return (
                            <div key={index} className='ContactCard' onClick={this.props.openModal}>
                                    <img className='PersonPhoto' src={this.props.contacts[contact].img} alt=''/>
                                    <b>{this.props.contacts[contact].name}</b>
                                    <p><b>Number:</b> {this.props.contacts[contact].number}</p>
                                    <p><b>E-mail:</b> {this.props.contacts[contact].email}</p>
                            </div>
                        )
                    })}
                </Fragment>
            </div>
        );
    }

    openModal() {

    }
}

const mapStateToProps =(state)=>({
    contacts: state.allContacts,
});
const mapDispatchToProps =(dispatch)=>({
    getContacts: () => dispatch(getContacts()),
    delContact: () => dispatch(delContact())
});

export default connect(mapStateToProps,mapDispatchToProps)(ContactList);