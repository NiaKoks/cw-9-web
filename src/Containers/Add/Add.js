import React, {Component} from 'react';
import AddForm from "../../Components/AddForm/AddForm";
import {connect} from "react-redux";
import {addContact} from "../../store/actions/actions";
import {NavLink} from "react-router-dom";
import './Add.css'
class Add extends Component {

    addContact = contact =>{
        this.props.addContact(contact).then(()=>{
            this.props.history.push('/')
        })
    };
    render() {
        return (
            <div>
                <h3>Add New contact</h3>
                <NavLink to='/' className="GoBackLink">Back to Contact Book</NavLink>
                <hr/>
                <AddForm onSubmit={this.addContact}/>
            </div>
        );
    }
}

const mapDispatchToProps=(dispatch)=>({
    addContact: contact => dispatch(addContact(contact))
})

export default connect(null,mapDispatchToProps)(Add);

