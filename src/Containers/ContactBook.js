import React, {Component} from 'react';
import ContactList from '../Components/ContactList/ContactList';
import './ContactBook.css'
import {NavLink} from "react-router-dom";
class ContactBook extends Component {


    render() {
        return (
            <div className='ContactBook'>
                <h3>Contacts</h3>
                <NavLink to="/add" className="AddContactLink">Add new Contact</NavLink>
                <hr/>
                <ContactList/>
            </div>
        );
    }
}

export default ContactBook;